#pragma once
#include<iostream>
#include<string>
#include<vector>

class Item;
using namespace std;

class Player
{
public:
	Player(int hp, int crystals, int rp, int pulls);
	~Player();

	// Methods
	void displayStats();
	void pullItem();
	void addPulledItems(Item* item);

	// Accesor
	int getHp();
	int getCrystals();
	int getRp();
	int getPulls();
	Item* getItem();
	vector<Item*> getPulledItems();

	void setHp(int hp);
	void setCrystals(int crystals);
	void setRp(int rp);
	void setPulls(int pulls);

private:
	int mHp;
	int mCrystals;
	int mRp;
	int mPulls;
	Item* mItem;
	vector<Item*> mPulledItems;
};

