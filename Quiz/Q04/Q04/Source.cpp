#include<iostream>
#include<string>
#include"Item.h"
#include"Player.h"
#include"Stats.h"
#include<time.h>	

using namespace std;
int main()
{
	srand(time(0));
	// Character Creation
	Player* player = new Player(100, 100, 0, 0);

	while (true)
	{
		// Display Stats
		player->displayStats();

		// Pull Item from Gacha
		player->pullItem();

		// 5 crystal fee 
		player->setCrystals(player->getCrystals() - 5);
		cout << endl;

		// Item Takes Effect
		player->getItem()->effect(player);

		// Add 1 to pull counter 
		player->setPulls(player->getPulls() + 1);

		// Add pulled items to player inventory
		player->addPulledItems(player->getItem());

		system("pause");
		system("cls");

		// Evaluator
		if (player->getRp() >= 100)
		{
			cout << "You Win !!!" << endl;
			break;
		}
		else if (player->getCrystals() == 0 || player->getHp() == 0)
		{
			cout << "You Lost !!!" << endl;
			break;
		}
	}

	cout << "========================" << endl;
	player->displayStats();
	cout << "========================" << endl;

	// Collate Items
	int rCount = 0;
	int srCount = 0;
	int ssrCount = 0;
	int crystalCount = 0;
	int bombCount = 0;
	int healCount = 0;
	cout << "Items Pulled" << endl;
	for (int i = 0; i < player->getPulledItems().size(); i++)
	{
		Item* item = player->getPulledItems()[i];
		if (item->getName() == "R") rCount++;
		else if (item->getName() == "SR") srCount++;
		else if (item->getName() == "SSR") ssrCount++;
		else if (item->getName() == "Bomb") bombCount++;
		else if (item->getName() == "Crystal") crystalCount++;
		else if (item->getName() == "Health Potion") healCount++;
	}
	cout << "------------------------" << endl;
	cout << "SSR: " << ssrCount << endl;
	cout << "SR: " << srCount << endl;
	cout << "R: " << rCount << endl;
	cout << "Bomb: " << bombCount << endl;
	cout << "Crystal: " << crystalCount << endl;
	cout << "Health Potion: " << healCount << endl;

	delete player;

	system("pause");
	return 0;
}