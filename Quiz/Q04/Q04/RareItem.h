#pragma once
#include "Item.h"
#include "Stats.h"

class Player;
using namespace std;

class RareItem :
	public Item
{
public:
	RareItem(string name, RarityType rare);

	void effect(Player* user) override;
private:
	RarityType mRare;
};

