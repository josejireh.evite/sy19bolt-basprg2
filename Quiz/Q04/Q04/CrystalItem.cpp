#include "CrystalItem.h"
#include "Player.h"

CrystalItem::CrystalItem()
	:Item("Crystal")
{
}

void CrystalItem::effect(Player * user)
{
	Item::effect(user);

	user->setCrystals(user->getCrystals() + 15);
	cout << "You recieved 15 crystals" << endl;
}
