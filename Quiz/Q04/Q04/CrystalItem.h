#pragma once
#include "Item.h"

class Player;
using namespace std;

class CrystalItem :
	public Item
{
public:
	CrystalItem();

	void effect(Player* user) override;
};

