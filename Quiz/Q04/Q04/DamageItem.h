#pragma once
#include "Item.h"

class Player;
using namespace std;

class DamageItem :
	public Item
{
public:
	DamageItem();

	void effect(Player* user) override;
};

