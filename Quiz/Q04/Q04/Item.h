#pragma once
#include<string>
#include<iostream>
using namespace std;

class Player;

class Item
{
public:
	Item(string name);
	virtual void effect(Player* user);
	void displayName();

	string getName();
	
private:
	string mName;
};

