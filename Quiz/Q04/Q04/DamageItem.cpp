#include "DamageItem.h"
#include "Player.h"
DamageItem::DamageItem()
	:Item("Bomb")
{
}

void DamageItem::effect(Player * user)
{
	Item::effect(user);

	user->setHp(user->getHp() - 25);
	cout << "You lose 25 HP" << endl;
}
