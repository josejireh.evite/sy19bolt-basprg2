#include "RareItem.h"
#include "Player.h"

RareItem::RareItem(string name, RarityType rare)
	: Item(name)
{
	mRare = rare;
}

void RareItem::effect(Player* user)
{
	Item::effect(user);

	switch (mRare)
	{
	case SSR:
		user->setRp(user->getRp() + 50);
		cout << "You recieved 50 Rarity Points" << endl;
		break;
	case SR:
		user->setRp(user->getRp() + 10);
		cout << "You recieved 10 Rarity Points" << endl;
		break;
	case R:
		user->setRp(user->getRp() + 1);
		cout << "You recieved 1 Rarity Points" << endl;
		break;
	default:
		break;
	}
}
