#include "Player.h"
#include"Item.h"
#include "CrystalItem.h"
#include "HealingItem.h"
#include "RareItem.h"
#include "DamageItem.h"
Player::Player(int hp, int crystals, int rp, int pulls)
{
	mHp = hp;
	mCrystals = crystals;
	mRp = rp;
	mPulls = pulls;
}

Player::~Player()
{
	for (int i = 0; i < mPulledItems.size(); i++)
	{
		delete mPulledItems[i];
	}
	mPulledItems.clear();
}

void Player::displayStats()
{
	cout << "HP :" << mHp << endl;
	cout << "Crystals: " << mCrystals << endl;
	cout << "Rarity Points: " << mRp << endl;
	cout << "Pulls: " << mPulls << endl;
}

void Player::pullItem()
{
	int random = rand() % 100 + 1;

	if (random == 1)
	{
		Item* item = new RareItem("SSR", SSR);
		mItem = item;
	}
	else if (random <= 9)
	{
		Item* item = new RareItem("SR", SR);
		mItem = item;
	}
	else if (random <= 40)
	{
		int random = rand() % 2;
		if (random == 0)
		{
			Item* item = new HealingItem();
			mItem = item;
		}
		else if (random == 1)
		{
			Item* item = new CrystalItem();
			mItem = item;
		}
	}
	else if (random <= 60)
	{
		Item* item = new DamageItem();
		mItem = item;
	}
	else if (random <= 100)
	{
		Item* item = new RareItem("R", R);
		mItem = item;
	}
}

void Player::addPulledItems(Item * item)
{
	mPulledItems.push_back(item);
}

int Player::getHp()
{
	return mHp;
}

int Player::getCrystals()
{
	return mCrystals;
}

int Player::getRp()
{
	return mRp;
}

int Player::getPulls()
{
	return mPulls;
}

Item * Player::getItem()
{
	return mItem;
}

vector<Item*> Player::getPulledItems()
{
	return mPulledItems;
}

void Player::setHp(int hp)
{
	mHp = hp;

	if (mHp < 0) mHp = 0;
}

void Player::setCrystals(int crystals)
{
	mCrystals = crystals;

	if (mCrystals < 0) mCrystals = 0;
}

void Player::setRp(int rp)
{
	mRp = rp;
}

void Player::setPulls(int pulls)
{
	mPulls = pulls;
}
