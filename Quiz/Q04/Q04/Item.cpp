#include "Item.h"

Item::Item(string name)
{
	mName = name;
}

void Item::effect(Player* user)
{
	cout <<"You pulled " << mName << endl;
}

void Item::displayName()
{
	cout << mName << endl;
}

string Item::getName()
{
	return mName;
}
