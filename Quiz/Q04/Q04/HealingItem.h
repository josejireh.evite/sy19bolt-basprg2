#pragma once
#include "Item.h"

class Player;
using namespace std;

class HealingItem :
	public Item
{
public:
	HealingItem();

	void effect(Player* user) override;
};

