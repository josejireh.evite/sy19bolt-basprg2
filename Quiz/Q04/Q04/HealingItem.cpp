#include "HealingItem.h"
#include "Player.h"

HealingItem::HealingItem()
	: Item("Health Potion")
{
}

void HealingItem::effect(Player * user)
{
	Item::effect(user);

	user->setHp(user->getHp() + 30);
	cout << "You healed 30HP" << endl;
}
