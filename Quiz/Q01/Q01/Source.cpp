#include<iostream>	
#include<vector>
#include<string>
#include<time.h>

using namespace std;

// Wager Function
int wager(int mmLeft)
{
	int wager;
	cout << "How much lenght are you going to wager? ";
	cin >> wager;
	while (true)
	{
		if (wager <= mmLeft && wager >= 1)
		{
			break;
		}
		else if (wager > mmLeft)
		{
			cout << "invalid wager, TRY AGAIN!!! (you can't wager mm that is beyond what you have)" << endl;
			cout << "How much lenght are you going to wager? ";
			cin >> wager;
		}
		else
		{
			cout << "invalid wager, TRY AGAIN!!!" << endl;
			cout << "How much lenght are you going to wager? ";
			cin >> wager;
		}
	}

	return wager;
}

// Create a deck Function
vector<string> theDeck(int role)
{
	vector<string> deck;
	if (role == 0) deck.push_back("Emperor");
	else if (role == 1) deck.push_back("Slave");
	deck.push_back("Civilian");
	deck.push_back("Civilian");
	deck.push_back("Civilian");
	deck.push_back("Civilian");

	return deck;
}

// Kaiji's Card Function
int playKaijiCard(vector<string> deck)
{
	cout << " You have: " << endl;
	int number = 1;
	for (int i = 0; i < deck.size(); i++)
	{
		cout << "|" << number << "|" << deck[i] << endl;
		number++;
	}

	cout << "Pick a card to play(Use the number on left side as input): ";
	int chosenCard;
	cin >> chosenCard;

	if (chosenCard == 5 || chosenCard == 4 || chosenCard == 3)
	{
		chosenCard = 2;
	}

	int outputHolder = chosenCard - 1;
	cout << "Kaiji plays " << deck[outputHolder] << " card" << endl;

	return chosenCard;
}

// Mr. Tonegawa Card Function
int playTonegawaCard(int role)
{
	int chosenCard = rand() % 5 + 1;
	if (chosenCard == 5 || chosenCard == 4 || chosenCard == 3)
	{
		chosenCard = 2;
	}

	if (chosenCard == 1)
	{
		if (role == 0) cout << "Tonegawa plays Slave card" << endl;
		else if (role == 1) cout << "Tonegawa plays Emperor card" << endl;
	}
	else if (chosenCard == 2)
	{
		cout << "Tonegawa plays Civilian Card" << endl;
	}
	return chosenCard;
}

// Added delete card function 
void deleteCard(vector<string>& deck, int chosenCard)
{
	if (chosenCard == 1)
	{
		deck.erase(deck.begin() + 0);
	}
	else if (chosenCard == 2)
	{
		deck.erase(deck.begin() + 1);
	}
}

// Evaluation of two cards
int evaluation(int kaijiChosenCard, int tonegawaChosenCard, int role, int& mmLeft, int wager, int& winnings)
{
	int evaluate = -1;
	// 0 win
	// 1 lose
	// 2 draw
	if (kaijiChosenCard == 2 && tonegawaChosenCard == 2)
	{
		evaluate = 2;
		cout << "Draw!!!" << endl;
	}

	// Evaluates Depending on the Role
	if (role == 0)
	{
		if (kaijiChosenCard == 1 && tonegawaChosenCard == 2)
		{
			evaluate = 0;
			cout << "Kaiji Wins!!! ";
		}
		else if (kaijiChosenCard == 1 && tonegawaChosenCard == 1)
		{
			evaluate = 1;
			cout << "Kaiji Loses ";
		}
		else if (kaijiChosenCard == 2 && tonegawaChosenCard == 1)
		{
			evaluate = 0;
			cout << "Kaiji Wins!!! ";
		}

		if (evaluate == 0)
		{
			wager *= 100000;
			winnings += wager;
			cout << "earned " << wager << endl;
		}
		else if (evaluate == 1)
		{
			mmLeft -= wager;
			cout << "The drill digs " << wager << "mm into Kaiji's ears" << endl;
		}
	}
	else if (role == 1)
	{
		if (kaijiChosenCard == 1 && tonegawaChosenCard == 2)
		{
			evaluate = 0;
			cout << "Kaiji Lose ";
		}
		else if (kaijiChosenCard == 1 && tonegawaChosenCard == 1)
		{
			evaluate = 1;
			cout << "Kaiji Wins!!! ";
		}
		else if (kaijiChosenCard == 2 && tonegawaChosenCard == 1)
		{
			evaluate = 0;
			cout << "Kaiji Lose ";
		}

		if (evaluate == 1)
		{
			wager *= 500000;
			winnings += wager;
			cout << "earned " << wager << endl;
		}
		else if (evaluate == 0)
		{
			mmLeft -= wager;
			cout << "The drill digs " << wager << "mm into Kaiji's ears" << endl;
		}
	}
	return evaluate;
}

// Play Round Function
void playRound(int round, int& mmLeft, int& moneyEarned, int role)
{
	int playerWager = wager(mmLeft);

	vector<string> kaijiDeck = theDeck(role);

	while (true)
	{
		system("cls");

		int kaijiChosenCard = playKaijiCard(kaijiDeck);
		deleteCard(kaijiDeck, kaijiChosenCard);

		int tonegawaChosenCard = playTonegawaCard(role);

		int result = evaluation(kaijiChosenCard, tonegawaChosenCard, role, mmLeft, playerWager, moneyEarned);

		system("pause");

		if (result == 0 || result == 1) break;
	}
}


int main()
{

	srand(time(0));

	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;
	int role = 0;
	bool condition = true;
	int gameEnding = -1;

	while (condition)
	{
		// Emperor Side = 0
		// Slave Side  = 1
		if (round == 4) role = 1;
		else if (round == 7) role = 0;
		else if (round == 10) role = 1;

		// Display
		cout << "Round: " << round << "/12" << endl;
		cout << "Money: " << moneyEarned << endl;
		cout << "Distance till eardrums are pierced: " << mmLeft << endl;
		if (role == 0) cout << "Side: Emperor" << endl;
		else if (role == 1) cout << "Side: Slave" << endl;
		cout << "	" << endl;
		cout << "	" << endl;
		cout << "	" << endl;

		// Playround caller
		playRound(round, mmLeft, moneyEarned, role);

		round++;
		system("cls");

		// Loop Evaluator
		if (round == 13)
		{
			condition = false;
			gameEnding = 1;
		}
		else if (moneyEarned >= 20000000)
		{
			condition = false;
			gameEnding = 0;
		}
		else if (mmLeft <= 0)
		{
			condition = false;
			gameEnding = 2;
		}
	}

	// Ending Output 
	if (gameEnding == 0)
	{
		cout << "Kaiji won over 20 million yen and avenged his friends" << endl;
	}
	else if (gameEnding == 1)
	{
		cout << "Kaiji's eardrums are safe but he didn't reach his goal :(" << endl;
	}
	else if (gameEnding == 2)
	{
		cout << "Mr. Tonegawa laughs as Kaiji's eardrums burst and bled out" << endl;
	}

	return 0;
}