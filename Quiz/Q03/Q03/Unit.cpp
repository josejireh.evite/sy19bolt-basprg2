#include "Unit.h"


Unit::Unit(string name, string unitClass, int hitpoint, int power, int vitality, int agility, int dexterity)
{
	mName = name;
	mClass = unitClass;
	mHitPoint = hitpoint;
	mPower = power;
	mVitality = vitality;
	mAgility = agility;
	mDexterity = dexterity;
	mMaxHitPoint = hitpoint;
}

void Unit::attack(Unit * target)
{
	// Hitrate Formula
	float dex = mDexterity;
	float enemyAgi = target->getAgility();

	int hitrate;
	hitrate = (dex / enemyAgi) * 100.0f;

	if (hitrate <= 20) hitrate = 20;
	else if (hitrate >= 80) hitrate = 80;

	int random = rand() % 100 + 1;

	// if statement if success or miss
	if (random > hitrate)
	{
		cout << mName << " missed ! " << endl;
		return;
	}
	else if (random <= hitrate)
	{
		// Damage Formula 
		// Warrior vs Any Class
		if (mClass == "Warrior" && target->mClass == "Warrior")
		{
			float power = mPower;
			float enemyVit = target->getVitality();

			int damage = (power - enemyVit) * 1.0f;
			if (damage < 1) damage = 1;

			target->setHitPoint(target->getHitPoint() - damage);
			cout << mName << " dealt " << damage << " damage " << " to " << target->getName() << endl;
		}
		else if (mClass == "Warrior" && target->mClass == "Assassin")
		{
			float power = mPower;
			float enemyVit = target->getVitality();

			int damage = (power - enemyVit) * 1.5f;
			if (damage < 1) damage = 1;

			target->setHitPoint(target->getHitPoint() - damage);
			cout << mName << " dealt " << damage << " damage " << " to " << target->getName() << endl;
		}
		else if (mClass == "Warrior" && target->mClass == "Mage")
		{
			float power = mPower;
			float enemyVit = target->getVitality();

			int damage = (power - enemyVit) * 0.5f;
			if (damage < 1) damage = 1;

			target->setHitPoint(target->getHitPoint() - damage);
			cout << mName << " dealt " << damage << " damage " << " to " << target->getName() << endl;
		}

		// Assassin vs Any Class
		if (mClass == "Assassin" && target->mClass == "Warrior")
		{
			float power = mPower;
			float enemyVit = target->getVitality();

			int damage = (power - enemyVit) * 0.5f;
			if (damage < 1) damage = 1;

			target->setHitPoint(target->getHitPoint() - damage);
			cout << mName << " dealt " << damage << " damage " << " to " << target->getName() << endl;
		}
		else if (mClass == "Assassin" && target->mClass == "Assassin")
		{
			float power = mPower;
			float enemyVit = target->getVitality();

			int damage = (power - enemyVit) * 1.0f;
			if (damage < 1) damage = 1;

			target->setHitPoint(target->getHitPoint() - damage);
			cout << mName << " dealt " << damage << " damage " << " to " << target->getName() << endl;
		}
		else if (mClass == "Assassin" && target->mClass == "Mage")
		{
			float power = mPower;
			float enemyVit = target->getVitality();

			int damage = (power - enemyVit) * 1.5f;
			if (damage < 1) damage = 1;

			target->setHitPoint(target->getHitPoint() - damage);
			cout << mName << " dealt " << damage << " damage " << " to " << target->getName() << endl;
		}

		// Mage vs Any Class
		if (mClass == "Mage" && target->mClass == "Warrior")
		{
			float power = mPower;
			float enemyVit = target->getVitality();

			int damage = (power - enemyVit) * 1.5f;
			if (damage < 1) damage = 1;

			target->setHitPoint(target->getHitPoint() - damage);
			cout << mName << " dealt " << damage << " damage " << " to " << target->getName() << endl;
		}
		else if (mClass == "Mage" && target->mClass == "Assassin")
		{
			float power = mPower;
			float enemyVit = target->getVitality();

			int damage = (power - enemyVit) * 0.5f;
			if (damage < 1) damage = 1;

			target->setHitPoint(target->getHitPoint() - damage);
			cout << mName << " dealt " << damage << " damage " << " to " << target->getName() << endl;
		}
		else if (mClass == "Mage" && target->mClass == "Mage")
		{
			float power = mPower;
			float enemyVit = target->getVitality();

			int damage = (power - enemyVit) * 1.0f;
			if (damage < 1) damage = 1;

			target->setHitPoint(target->getHitPoint() - damage);
			cout << mName << " dealt " << damage << " damage " << " to " << target->getName() << endl;
		}
	}
}

string Unit::getName()
{
	return mName;
}

string Unit::getClass()
{
	return mClass;
}

int Unit::getHitPoint()
{
	return mHitPoint;
}

int Unit::getMaxHitPoint()
{
	return mMaxHitPoint;
}

int Unit::getPower()
{
	return mPower;
}

int Unit::getVitality()
{
	return mVitality;
}

int Unit::getAgility()
{
	return mAgility;
}

int Unit::getDexterity()
{
	return mDexterity;
}

void Unit::setName(string name)
{
	mName = name;
}

void Unit::setHitPoint(int value)
{
	mHitPoint = value;
	if (mHitPoint < 0) mHitPoint = 0;
}

void Unit::setMaxHitPoint(int value)
{
	mMaxHitPoint = value;
}

void Unit::setPower(int value)
{
	mPower = value;
}

void Unit::setVitality(int value)
{
	mVitality = value;
}

void Unit::setAgility(int value)
{
	mAgility = value;
}

void Unit::setDexterity(int value)
{
	mDexterity = value;
}



