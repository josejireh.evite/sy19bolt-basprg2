#include <string>
#include <time.h>
#include <iostream>
#include "Unit.h"
using namespace std;

// Creating the player's character Function
Unit* characterCreation()
{
	cout << "What is your name?";
	string name;
	cin >> name;

	cout << "Pick a class" << endl;
	cout << " [1] " << "Warrior" << endl;
	cout << " [2] " << "Assassin" << endl;
	cout << " [3] " << "Mage" << endl;
	int aClass;
	cin >> aClass;

	Unit* player = nullptr;
	if (aClass == 1)
	{
		player = new Unit(name, "Warrior", 30, 6, 6, 6, 6);
	}
	else if (aClass == 2)
	{
		player = new Unit(name, "Assassin", 30, 6, 4, 8, 8);
	}
	else if (aClass == 3)
	{
		player = new Unit(name, "Mage", 30, 8, 4, 6, 6);
	}
	return player;
}

// Print Player Stats Function
void printPlayerStats(Unit* player)
{
	cout << "Name: " << player->getName() << endl;
	cout << "Class: " << player->getClass() << endl;
	cout << "HP: " << player->getHitPoint() << endl;
	cout << "Power: " << player->getPower() << endl;
	cout << "Vitality: " << player->getVitality() << endl;
	cout << "Dexterity: " << player->getDexterity() << endl;
	cout << "Agility: " << player->getAgility() << endl;
}

// Enemy Spawn Function
Unit* enemySpawner(int fight, int add)
{
	int random = rand() % 3;
	Unit* enemy = nullptr;
	if (random == 0)
	{
		enemy = new Unit("Enemy Warrior", "Warrior", 15, 2, 2, 2, 2);
	}
	if (random == 1)
	{
		enemy = new Unit("Enemy Assassin", "Assassin", 15, 2, 1, 4, 4);
	}
	if (random == 2)
	{
		enemy = new Unit("Enemy Mage", "Mage", 15, 4, 1, 2, 2);
	}

	if (fight >  1)
	{
		enemy->setHitPoint(enemy->getHitPoint() + add);
		enemy->setPower(enemy->getPower() + add);
		enemy->setVitality(enemy->getVitality() + add);
		enemy->setAgility(enemy->getAgility() + add);
		enemy->setDexterity(enemy->getDexterity() + add);
	}

	return enemy;
}

int main()
{
	srand(time(0));

	//Creating player character
	Unit* player = characterCreation();
	
	int fight = 1;
	int statAdd = 0;
	while (true)
	{
		system("cls");

		// Spawn Enemy
		Unit* enemy = enemySpawner(fight, statAdd);

		// HUD
		cout << "Fight " << fight << endl;
		cout << "========" << endl;

		// Print Player Stat
		printPlayerStats(player);

		cout << endl;
		cout << "   VS   " << endl;
		cout << endl;

		// Print Enemy Stat
		printPlayerStats(enemy);
		system("pause");

		// Attack w/ Turn Order via Agility
		while (true)
		{
			system("cls");
			cout << player->getName() << "'s HP[" << player->getHitPoint() << "]" << endl;
			cout << enemy->getName() << "'s HP[" << enemy->getHitPoint() << "]" << endl;
			cout << endl;
			if (player->getAgility() == enemy->getAgility())
			{
				player->attack(enemy);
				enemy->attack(player);
				system("pause");
			}
			else if (player->getAgility() > enemy->getAgility())
			{
				player->attack(enemy);
				enemy->attack(player);
				system("pause");
			}
			else if (player->getAgility() < enemy->getAgility())
			{
				enemy->attack(player);
				player->attack(enemy);
				system("pause");
			}
			cout << endl;
			if (player->getHitPoint() == 0) break;
			else if (enemy->getHitPoint() == 0) break;
		}

		system("cls");

		// Death of evaluate and stat rewards
		if (player->getHitPoint() == 0) break;
		else if (enemy->getHitPoint() == 0)
		{
			cout << "Combat Ended!!!" << endl;
			cout << player->getName() << "'s stats increased: ";
			if (enemy->getClass() == "Warrior")
			{
				player->setPower(player->getPower() + 3);
				player->setVitality(player->getVitality() + 3);
				cout << "Power + 3 & Vitality + 3" << endl;
			}
			else if(enemy->getClass() == "Assassin")
			{
				player->setAgility(player->getAgility() + 3);
				player->setDexterity(player->getDexterity() + 3);
				cout << "Agility + 3 & Dexterity + 3" << endl;
			}
			else if (enemy->getClass() == "Mage")
			{
				player->setPower(player->getPower() + 5);
				cout << "Power + 5" << endl;
			}
		}

		// Deallocation of  Enemy 
		delete enemy;

		// Healing 
		float floatyNumber = player->getMaxHitPoint();
		floatyNumber *= 0.3f;
		player->setHitPoint(player->getHitPoint() + floatyNumber);

		// Convert to Int for display
		int convertedFloatyNumber = (int)floatyNumber;
		cout << player->getName() << " healed " << convertedFloatyNumber << " HP " << endl;

		// Increments 
		fight++;
		statAdd += 3;
		system("pause");
	}

	// Player Death
	system("cls");
	cout << player->getName() << " died!" << endl;
	cout << "Fights: " << fight << endl;
	printPlayerStats(player);

	// Deallocation for player
	delete player;

	system("pause");
	return 0;
}