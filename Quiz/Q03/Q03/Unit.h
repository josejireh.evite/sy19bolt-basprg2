#pragma once
#include <string>
#include <iostream>

using namespace std;

class Unit
{
public:
	// Constructors
	Unit(string name, string unitClass, int hitpoint, int power, int vitality, int agility, int dexterity);

	//No need for destructors 

	// Methods
	void attack(Unit* target);

	// Accessors
	string getName();
	string getClass();
	int getHitPoint();
	int getMaxHitPoint();
	int getPower();
	int getVitality();
	int getAgility();
	int getDexterity();

	void setName(string name);
	// No need for setter for mClass
	void setHitPoint(int value);
	void setMaxHitPoint(int value);
	void setPower(int value);
	void setVitality(int value);
	void setAgility(int value);
	void setDexterity(int value);

private:
	string mName;
	string mClass;
	int mHitPoint;
	int mMaxHitPoint;
	int mPower;
	int mVitality;
	int mAgility;
	int mDexterity;

};

