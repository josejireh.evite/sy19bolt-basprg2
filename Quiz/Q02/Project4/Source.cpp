#include "Node.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

// Creating Circular Linked List upgraded 
Node* creatingList(int size)
{
	Node* head = nullptr;
	Node* current = nullptr;
	Node* previous = nullptr;

	string soldierName;
	for (int i = 1; i <= size; i++)
	{
		if (i == 1)
		{
			Node* current = new Node;
			cout << "What's You Name Soldier? ";
			cin >> soldierName;
			current->name = soldierName;
			head = current;
			previous = current;
			continue;
		}
		else if (i == size)
		{
			Node* current = new Node;
			cout << "What's You Name Soldier? ";
			cin >> soldierName;
			current->name = soldierName;
			previous->next = current;
			current->next = head;
			continue;
		}
		Node* current = new Node;
		cout << "What's You Name Soldier? ";
		cin >> soldierName;
		current->name = soldierName;
		previous->next = current;
		previous = current;


	}
	return head;
}

// Print List Function
void printList(Node* list, int size)
{
	Node* nodePrinterPointer = list;
	for (int i = 0; i < size; i++)
	{
		cout << nodePrinterPointer->name << endl;
		nodePrinterPointer = nodePrinterPointer->next;
	}
}

// Function to randmoze the starting point of list
Node* startingPoint(Node* list, int size)
{
	int randomNumber = rand() % size;
	for (int i = 0; i < randomNumber; i++)
	{
		list = list->next;
	}
	return list;
}

// Pass cloak function
Node* passDemCloak(Node* list, int size)
{
	int randomNumber = rand() % size;
	cout << " " << endl;
	cout << "Result: " << endl;
	int displayNumber = randomNumber + 1;
	cout << list->name << " has drawn " << displayNumber << endl;

	// Pass the cloak, -1 to offset the count for the holder
	int count = -1;
	while (count != randomNumber)
	{
		list = list->next;
		count++;
	}
	cout << list->name << " was eliminated " << endl;
	cout << " " << endl;

	return list;
}

// Delete Node Function
Node* deleteNode(Node* list, int& size)
{
	Node* toDelete = list;
	Node* previous = nullptr;

	for (int i = 0; i < size; i++)
	{
		previous = toDelete;
		toDelete = toDelete->next;
	}
	previous->next = toDelete->next;

	// offset the data to reach the cloak holder
	previous = previous->next;

	size -= 1;

	delete toDelete;
	return previous;
}

int main()
{
	srand(time(0));
	cout << "How many you units do you want? ";
	int size;
	cin >> size;

	// Creating Circular Linked List upgraded
	Node* head = new Node;
	head = creatingList(size);

	// Randomize the starting point of List
	head = startingPoint(head, size);

	int round = 1;
	while (size != 1)
	{
		// Hud
		cout << "==============" << endl;
		cout << "ROUND " << round << endl;
		cout << "==============" << endl;
		printList(head, size);

		// Step 3 and 5
		head = passDemCloak(head, size);
		head = deleteNode(head, size);

		round++;
		system("pause");
	}

	// Displaying the Winner
	cout << "==============" << endl;
	cout << "FINAL RESULTS" << endl;
	cout << "==============" << endl;
	cout << head->name << " will go to seek for reinforcements " << endl;

	system("pause");
	return 0;
}