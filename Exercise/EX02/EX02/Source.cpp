#include <iostream>
#include <string>	
#include <vector>
#include <time.h>

using namespace std;

// Inventory Fill
void populateVector(vector<string>& vector)
{
	for (int i = 0; i < 10; i++)
	{
		int randomizer = rand() % 4;
		if (randomizer == 0)
		{
			vector.push_back("RedPotion");
		}
		else if (randomizer == 1)
		{
			vector.push_back("Elixir");
		}
		else if (randomizer == 2)
		{
			vector.push_back("EmptyBottle");
		}
		else if (randomizer == 3)
		{
			vector.push_back("BluePotion");
		}
	}
}

// Display Inventory
void DisplayInventory(const vector<string>& vector)
{
	for (int i = 0; i < vector.size() ; i++)
	{
		cout << vector[i] << endl;
	}
}

// Count Item
void countItem(const vector<string>& vector, int& cRedPotion, int&cElixir, int& cEmptyBottle, int& cBluePotion)
{
	for (int i = 0; i < vector.size(); i++)
	{
		if (vector[i] == "RedPotion")
		{
			cRedPotion++;
		}
		else if (vector[i] == "Elixir")
		{
			cElixir++;
		}
		else if (vector[i] == "EmptyBottle")
		{
			cEmptyBottle++;
		}
		else if (vector[i] == "BluePotion")
		{
			cBluePotion++;
		}
	}
}

// Remove Item
void removeItem(vector<string>& vector)
{
	int anyNumber;
	
	cout << "What index do you want to remove, 0 - 9 ?" << endl;
	cin >> anyNumber;
	bool stop = true;
	while (stop)
	{
		if (anyNumber < 0 || anyNumber > 9)
		{
			cout << "invalid Input pls try again" << endl;
			cout << "What index do you want to remove, 0 - 9 ?" << endl;
			cin >> anyNumber;
		}
		else
		{
			stop = false;
		}
	}
	vector.erase(vector.begin() + anyNumber);
	cout << "items index no. " << anyNumber << " has been removed" << endl;
}

int main()
{
	srand(time(0));
	vector<string> items;

	// Populate Function
	populateVector(items);

	// Printing of items
	for (int i = 0; i < items.size() ; i++)
	{
		cout << items[i] << endl;
	}
	system("pause");
	cout << "---------------------------------" << endl;
	cout << "---------------------------------" << endl;

	// Display Inventory Function
	DisplayInventory(items);
	cout << "---------------------------------" << endl;
	cout << "---------------------------------" << endl;

	// Count item Function
	int countRedPotion = 0;
	int countElixir = 0;
	int countEmptyBottle = 0;
	int countBluePotion = 0;

	countItem(items, countRedPotion, countElixir, countEmptyBottle, countBluePotion);
	cout << "The inventory has:" << endl;
	cout << countRedPotion << " " << "RedPotion" << endl;
	cout << countElixir << " " << "Elixir" << endl;
	cout << countEmptyBottle << " " << "EmptyBottle" << endl;
	cout << countBluePotion << " " << "BluePotion" << endl;
	cout << "---------------------------------" << endl;
	cout << "---------------------------------" << endl;

	// Remove item Function
	system("pause");
	removeItem(items);

	system("pause");
	return 0;
}