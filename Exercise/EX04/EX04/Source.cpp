#include<iostream>
#include<string>	
#include<time.h>	
#include "Source.h"

using namespace std;

struct Item
{
	string mithrilOre;
	string sharpTalon;
	string thickLeather;
	string jellopy;
	string cursedStone;
	int gold;
};

string* generateRandomItem()
{
	string itemPool[]
	{
		"mithrilOre",
		"sharpTalon",
		"thickLeather",
		"jellopy",
		"cursedStone"
	};

	string itemPooled = itemPool[rand() % 5];
	string* item = &itemPooled;

	return item;
}

void enterDungeon(int& gold)
{
	int multiplier = 1;
	gold -= 25;
	int dungeonGold = 0;
	
	string itemPool[]
	{
		"mithrilOre",
		"sharpTalon",
		"thickLeather",
		"jellopy",
		"cursedStone"
	};
	
	bool loop = true;
	while (loop)
	{
		string item = itemPool[rand() % 5];
		cout << item << endl;
		if (item == "mithrilOre")
		{
			dungeonGold += 100;
			cout << "Do you want to keep Looting? " << endl;
		}
		else if (item == "sharpTalon")
		{
			dungeonGold += 50;
			cout << "Do you want to keep Looting? " << endl;

		}
		else if (item == "thickLeather")
		{
			dungeonGold += 25;
			cout << "Do you want to keep Looting? " << endl;

		}
		else if (item == "jellopy")
		{
			dungeonGold += 5;
			cout << "Do you want to keep Looting? " << endl;

		}
		else if (item == "cursedStone")
		{
			cout << "You died, you lost your dungeon loot" << endl;
			loop = false;
		}

		string playerAnswer;
		cin >> playerAnswer;
		if (playerAnswer == "yes")
		{
			multiplier++;
		}
		else if (playerAnswer == "no")
		{
			dungeonGold *= multiplier;
			gold += dungeonGold;
			loop = false;
		}
	}

}

int main()
{
	srand(time(0));
	string* itemHolder;
	itemHolder = generateRandomItem();

	int userGold = 50;
	while (true)
	{
		if (userGold >= 500)
		{
			cout << "The Player won!!!" << endl;
			break;
		}
		else if (userGold >= 25 || userGold < 500)
		{
			enterDungeon(userGold);
		}
		else if (userGold < 25)
		{
			cout << "Not enough gold to enter Dungeon, Player Lose" << endl;
			break;
		}
	}


	system("pause");
	return 0;
}

