#include<string>
#include<iostream>
#include "Wizard.h"
#include "Spell.h"
#include<time.h>

using namespace std;

int main()
{
	srand(time(0));

	Wizard* wizard1 = new Wizard("Wizard 1", 250, 0, 10, 15);

	Wizard* wizard2 = new Wizard("Wizard 2", 250, 0, 10, 15);


	Spell* fireball = new Spell("Fireball", 40, 60, 50);

	int evaluator = -1;
	while (true)
	{
		// HUD
		cout << wizard1->getName() << endl;
		cout << "HP: " << wizard1->getHp()<< endl;
		cout << "MP: " << wizard1->getMp() << endl;
		cout << "                      " << endl;
		cout << "                      " << endl;
		cout << wizard2->getName() << endl;
		cout << "HP: " << wizard2->getHp() << endl;
		cout << "MP: " << wizard2->getMp() << endl;
		cout << "                      " << endl;
		cout << "                      " << endl;
	
		// Attack and Fireballs
		fireball->activate(wizard1, wizard2);
		fireball->activate(wizard2, wizard1);
		cout << "                      " << endl;
		cout << "                      " << endl;
		wizard1->attackWizard(wizard2);
		wizard2->attackWizard(wizard1);
	
		system("pause");
		system("cls");
		// Death Evaluator
		if (wizard1->getHp() == 0 && wizard2->getHp() == 0)
		{
			evaluator = 0;
			break;
		}
		else if (wizard1->getHp() == 0)
		{
			evaluator = 1;
			break;
		}
		else if (wizard2->getHp() == 0)
		{
			evaluator = 2;
			break;

		}
	}
	// Ending Screen
	system("cls");
	if (evaluator == 0)
	{
		cout << "Both Wizards died" << endl;
	}
	else if (evaluator == 1)
	{
		cout << wizard1->getName() << "died" << endl;
	}
	else if (evaluator == 2)
	{
		cout << wizard2->getName() << "died" << endl;
	}

	delete wizard1;
	delete wizard2;
	delete fireball;

	
	system("pause");
	return 0;
}