#pragma once
#include<string>
#include<iostream>
using namespace std;

class Wizard;	

class Spell
{
public:
	// Constructor
	Spell(string name, int mindamage, int maxdamage, int mpcost);

	void activate(Wizard* caster, Wizard* target);

	// Accessor 
	string getName();
	int getminDamage();
	int getMaxDamage();
	int getMpCost();

	void setName(string name);
	void setMinDamage(int value);
	void setMaxDamage(int value);
	void setMpCost(int value);

private:
	string mName;
	int mMinDamage;
	int mMaxDamage;
	int mMpCost;
};

