#pragma once
#include<string>

using namespace std;

class Spell;

class Wizard
{
public:
	// Constructor
	Wizard();
	Wizard(string name, int hp, int mp, int mindamage, int maxdamage);
	~Wizard();

	void attackWizard(Wizard* target);

	// Accessor
	string getName();
	int getHp();
	int getMp();
	int getMinDamage();
	int getMaxDamage();
	Spell* getSpell();

	void setName(string name);
	void setHp(int value);
	void setMp(int value);
	void setMinDamage(int damage);
	void setMaxDamage(int damage);
	void setSpell(Spell* spell);


private:
	string mName;
	int mHp;
	int mMp;
	int mMinDamage;
	int mMaxDamage;
	Spell* mSpell;
};

