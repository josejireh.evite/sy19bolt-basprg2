#include "Wizard.h"
#include "Spell.h"


Wizard::Wizard()
{
	mName = "Weak Wizard";
	mHp = 1;
	mMp = 0; 
	mMinDamage = 1;
	mMaxDamage = 1;

	mSpell = new Spell("Throw Rock", 1, 2, 10);
}

Wizard::Wizard(string name, int hp, int mp, int mindamage, int maxdamage)
{
	mName = name;
	mHp = hp;
	mMp = mp;
	mMinDamage = mindamage;
	mMaxDamage = maxdamage;

	mSpell = new Spell("Throw Rock", 1, 2, 10);
}

Wizard::~Wizard()
{
	delete mSpell;
}

void Wizard::attackWizard(Wizard* target)
{
	int randomizer = rand() % 2;
	if (randomizer == 0)
	{
		target->setHp(target->getHp() - mMinDamage);
		cout << mName << " normal attacks " << target->getName() << " for " << mMinDamage << " damage " << endl;
		int randomNumber = rand() % 2;
		if (randomNumber == 0)
		{
			mMp += 10;
			cout << mName << " gained " << 10 << " mana " << endl;
		}
		else if (randomNumber == 1)
		{
			mMp += 20;
			cout << mName << " gained " << 20 << " mana " << endl;
		}
		
	}
	else if (randomizer == 1)
	{
		target->setHp(target->getHp() - mMaxDamage);
		cout << mName << " normal attacks " << target->getName() << " for " << mMaxDamage << " damage " << endl;
		int randomNumber = rand() % 2;
		if (randomNumber == 0)
		{
			mMp += 10;
			cout << mName << " gained " << 10 << " mana " << endl;
		}
		else if (randomNumber == 1)
		{
			mMp += 20;
			cout << mName << " gained " << 20 << " mana " << endl;
		}
	}
}

string Wizard::getName()
{
	return mName;
}

int Wizard::getHp()
{
	return mHp;
}

int Wizard::getMp()
{
	return mMp;
}

int Wizard::getMinDamage()
{
	return mMinDamage;
}

int Wizard::getMaxDamage()
{
	return mMaxDamage;
}

Spell * Wizard::getSpell()
{
	return mSpell;
}

void Wizard::setName(string name)
{
	mName = name;
}

void Wizard::setHp(int value)
{
	mHp = value;

	if (mHp < 0) mHp = 0;
}

void Wizard::setMp(int value)
{
	mMp = value;

	if (mMp < 0) mMp = 0;
}

void Wizard::setMinDamage(int damage)
{
	mMinDamage = damage;
}

void Wizard::setMaxDamage(int damage)
{
	mMaxDamage = damage;
}

void Wizard::setSpell(Spell* spell)
{
	mSpell = spell;
}
