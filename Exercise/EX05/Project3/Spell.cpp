#include "Spell.h"
#include "Wizard.h"

Spell::Spell(string name, int mindamage, int maxdamage, int mpcost)
{
	mName = name;
	mMinDamage = mindamage;
	mMaxDamage = maxdamage;
	mMpCost = mpcost;
}

void Spell::activate(Wizard* caster, Wizard* target)
{
	if (caster->getMp() < 50)
	{
		return;
	}
	else if (caster->getMp() >= 50)
	{
		int randomizer = rand() % 2;
		if (randomizer == 0)
		{
			target->setHp(target->getHp() - mMinDamage);
			cout << caster->getName() << " cast " << mName << endl;
			cout << target->getName() << " will take " << mMinDamage << " damage" << endl;
		}
		else if (randomizer == 1)
		{
			target->setHp(target->getHp() - mMaxDamage);
			cout << caster->getName() << " cast " << mName << endl;
			cout << target->getName() << " will take " << mMaxDamage << " damage" << endl;
		}
		caster->setMp(caster->getMp() - 50);
	}
}

string Spell::getName()
{
	return mName;
}

int Spell::getminDamage()
{
	return mMinDamage;
}

int Spell::getMaxDamage()
{
	return mMaxDamage;
}

int Spell::getMpCost()
{
	return mMpCost;
}

void Spell::setName(string name)
{
	mName = name;
}

void Spell::setMinDamage(int value)
{
	mMinDamage = value;
}

void Spell::setMaxDamage(int value)
{
	mMaxDamage = value;
}

void Spell::setMpCost(int value)
{
	mMpCost = value;

	if (mMpCost < 0) mMpCost = 0;
}
