#include "Concentration.h"
#include "Unit.h"
Concentration::Concentration()
	:Skill("CONCENTRATION")
{
}

void Concentration::effect(Unit * caster)
{
	cout << caster->getName() << " casts " << this->getName() << ", dexterity is increased by 2!!!" << endl;
	caster->setDexterity(caster->getDexterity() + 2);
}
