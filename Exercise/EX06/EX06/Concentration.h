#pragma once
#include "Skill.h"

class Unit;

class Concentration :
	public Skill
{
public:
	Concentration();
	void effect(Unit* caster) override;
};
