#include <iostream>
#include <string>
#include "Unit.h"
#include "Skill.h"
#include "Heal.h"

using namespace std;

int main()
{
	Unit* overlord = new Unit("Ainz[Sorceror King]", 100, 50, 25, 10, 10);
	for (int i = 0; i < 5; i++)
	{
		cout << overlord->getName() << " is preparing for battle!!!" << endl;
	}
	system("pause");
	while (true)
	{
		system("cls");
		overlord->printStats();
		overlord->randomSkill();
		cout << endl;
		overlord->castSkill();
		delete overlord->getSkill();
		system("pause");
	}
	delete overlord;
	return 0;
}