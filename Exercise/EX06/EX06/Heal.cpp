#include "Heal.h"
#include "Unit.h"
Heal::Heal()
	:Skill("HEAL")
{
}

void Heal::effect(Unit * caster)
{
	cout << caster->getName() << " casts " << this->getName() << " and healed 10 HP!!!" << endl;
	caster->setHitPoint(caster->getHitPoint() + 10);
}
