#pragma once
#include "Skill.h"

class Unit;

class Heal :
	public Skill
{
public:
	Heal();
	void effect(Unit* caster) override;
};

