#include "Haste.h"
#include "Unit.h"
Haste::Haste()
	:Skill("HASTE")
{
}

void Haste::effect(Unit * caster)
{
	cout << caster->getName() << " casts " << this->getName() << ", agility is increased by 2!!!" << endl;
	caster->setAgility(caster->getAgility() + 2);
}
