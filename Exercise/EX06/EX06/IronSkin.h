#pragma once
#include "Skill.h"

class Unit;

class IronSkin :
	public Skill
{
public:
	IronSkin();
	void effect(Unit* caster) override;
};


