#include "IronSkin.h"
#include "Unit.h"
IronSkin::IronSkin()
	:Skill("IRON SKIN")
{
}

void IronSkin::effect(Unit * caster)
{
	cout << caster->getName() << " casts " << this->getName() << ", vitality is increased by 2!!!" << endl;
	caster->setVitality(caster->getVitality() + 2);
}
