#include "Unit.h"
#include "Skill.h"
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"

Unit::Unit(string name, int hp, int pow, int vit, int dex, int agi)
{
	mName = name;
	mHitPoint = hp;
	mPower = pow;
	mVitality = vit;
	mDexterity = dex;
	mAgility = agi;
}

Unit::~Unit()
{
	delete mSkill;
}

void Unit::printStats()
{
	cout << "Name: " << mName << endl;
	cout << "HP: " << mHitPoint << endl;
	cout << "POW: " << mPower << endl;
	cout << "VIT: " << mVitality << endl;
	cout << "DEX: " << mDexterity << endl;
	cout << "AGI: " << mAgility << endl;
}

void Unit::randomSkill()
{
	int random = rand() % 5;
	if (random == 0)
	{
		Skill* skill = new Heal();
		mSkill = skill;
	}
	else if (random == 1)
	{
		Skill* skill = new Might();
		mSkill = skill;
	}
	else if (random == 2)
	{
		Skill* skill = new IronSkin();
		mSkill = skill;
	}
	else if (random == 3)
	{
		Skill* skill = new Concentration();
		mSkill = skill;
	}
	else if (random == 4)
	{
		Skill* skill = new Haste();
		mSkill = skill;
	}
}

void Unit::castSkill()
{
	mSkill->effect(this);
}

string Unit::getName()
{
	return mName;
}

int Unit::getHitPoint()
{
	return mHitPoint;
}

int Unit::getPower()
{
	return mPower;
}

int Unit::getVitality()
{
	return mVitality;
}

int Unit::getDexterity()
{
	return mDexterity;
}

int Unit::getAgility()
{
	return mAgility;
}

Skill * Unit::getSkill()
{
	return mSkill;
}

void Unit::setName(string name)
{
	mName = name;
}

void Unit::setHitPoint(int hp)
{
	mHitPoint = hp;
	if (mHitPoint < 0) mHitPoint = 0;
}

void Unit::setPower(int power)
{
	mPower = power;
}

void Unit::setVitality(int vit)
{
	mVitality = vit;
}

void Unit::setDexterity(int dex)
{
	mDexterity = dex;
}

void Unit::setAgility(int agi)
{
	mAgility = agi;
}
