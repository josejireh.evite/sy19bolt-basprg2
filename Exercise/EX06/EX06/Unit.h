#pragma once
#include <string>	
#include <iostream>
using namespace std;

class Skill;

class Unit
{
public:
	// Constructor
	Unit(string name, int hp, int pow, int vit, int dex, int agi);
	
	// Destructor
	~Unit();

	// Methods
	void printStats();
	void randomSkill();
	void castSkill();

	// Accessor 
	string getName();
	int getHitPoint();
	int getPower();
	int getVitality();
	int getDexterity();
	int getAgility();
	Skill* getSkill();

	void setName(string name);
	void setHitPoint(int hp);
	void setPower(int pow);
	void setVitality(int vit);
	void setDexterity(int dex);
	void setAgility(int agi);

private:
	string mName;
	int mHitPoint;
	int mPower;
	int mVitality;
	int mDexterity;
	int mAgility;
	Skill* mSkill;
};

