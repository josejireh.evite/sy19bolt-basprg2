#pragma once
#include <string>
#include <iostream>
using namespace std;

class Unit;

class Skill
{
public:
	Skill(string name);
	virtual void effect(Unit* caster) = 0;

	string getName();

private:
	string mName;
};

