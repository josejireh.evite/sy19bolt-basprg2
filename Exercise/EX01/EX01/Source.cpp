#include <iostream>
#include <string>
#include <time.h>

using namespace std;

// Bet Function Version 1
int bet(int& userGold)
{
	int goldBet;
	cout << "How much will you bet: ";
	cin >> goldBet;
	
	bool betLoop = true;
	while (betLoop)
	{
		if (goldBet <= userGold && goldBet > 0)
		{
			betLoop = false;
		}
		else if (goldBet <= 0 || goldBet > userGold)
		{
			cout << "Invalid Input, it must be greater than 0 and less than the max gold" << endl;
			cout << "Try again, How much  will you bet: ";
			cin >> goldBet;
		}
	}

	userGold -= goldBet;

	return goldBet;
}

// Bet Function Version 2 
void betv2(int& userGold, int& goldBet)
{
	cout << "How much will you bet: ";
	cin >> goldBet;

	bool betLoop = true;
	while (betLoop)
	{
		if (goldBet < userGold && goldBet > 0)
		{
			betLoop = false;
		}
		else if (goldBet <= 0 || goldBet > userGold)
		{
			cout << "Invalid Input, it must be greater than 0 and less than the max gold" << endl;
			cout << "Try again, How much  will you bet: ";
			cin >> goldBet;	
		}
	}

	userGold -= goldBet;
}

// Dice Roll Function
int diceRoll(int& diceUno, int& diceDos)
{
	diceUno = rand() % 6 + 1 ;
	cout << diceUno << endl;
	diceDos = rand() % 6 + 1 ;
	cout << diceDos << endl;
	int sum = diceUno + diceDos;
	return sum;
}

// Payout Function
void payout(int& userGold, int userRoll, int enemyRoll, int playerBet, int dice1, int dice2, int enemydice1, int enemydice2)
{
	if (dice1 == 1 && dice2 == 1)
	{
		if (enemydice1 == 1 && enemydice2 == 1)
		{
			userGold += playerBet;
			cout << "Both Player and AI drew snake eyes, its a draw" << endl;
		}
		else
		{
			playerBet *= 3;
			userGold += playerBet;
			cout << "Player drew snake eyes, he recieves thrice his bet" << endl;
		}
	}
	else if (userRoll > enemyRoll)
	{
		cout << "Player wins his bet!!!" << endl;
		playerBet *= 2;
		userGold += playerBet;
	}
	else if (userRoll == enemyRoll)
	{
		cout << "Its a draw!!!" << endl;
		userGold += playerBet;
	}
	else if (userRoll < enemyRoll)
	{
		cout << "You lose your bet!!!" << endl;	
	}
	if (userGold <= 0) userGold = 0;
	cout << "----------------------------------------" << endl;
	cout << "The Player now has " << userGold << "!!!" << endl;
	cout << "----------------------------------------" << endl;
}

int main()
{
	srand(time(0));
	int playerGold = 1000;
	int playerBet = -1;
	int playerDiceOne;
	int playerDiceTwo;
	int aiDiceOne;
	int aiDiceTwo;
	bool exit = true;

	while (exit)
	{
		// Player Bet Function using bet version 1
		playerBet = bet(playerGold);
		cout << "The player now has: " << playerGold << endl;
		cout << "---------------------------------------" << endl;

		// Dice Roll of Player and AI
		cout << "The Player will roll his dice!!!" << endl;
		system("pause");
		int PlayerDiceSum = diceRoll(playerDiceOne, playerDiceTwo);
		cout << "Player rolls: " << PlayerDiceSum << endl;

		cout << "The AI will roll his dice!!!" << endl;
		system("pause");
		int aiDiceSum = diceRoll(aiDiceOne, aiDiceTwo);
		cout << "The AI rolls: " << aiDiceSum << endl;

		// Payout 
		payout(playerGold, PlayerDiceSum, aiDiceSum, playerBet, playerDiceOne, playerDiceTwo, aiDiceOne, aiDiceTwo);

		if (playerGold <= 0)
		{
			cout << "Game over, you are out of Gold!!!" << endl;
			exit = false;
		}
	}
	system("pause");
	return 0;
}