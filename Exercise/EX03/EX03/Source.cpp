#include<string>
#include<iostream>
#include<time.h>

using namespace std;

// Array as Pointer 3-1
void pointerArray(int array[])
{
	for (int i = 0; i < 10; i++)
	{
		array[i] = rand() % 101;
	}
}

// Creating Dynamic Array 3-2
int* dynamicArray()
{
	int* theArray = new int[10];

	for (int i = 0; i < 10; i++)
	{
		theArray[i] = rand() % 51;
	}

	for (int i = 0; i < 10; i++)
	{
		cout << theArray[i] << endl;
	}

	return theArray;
}

// Deleting dynamic elements
int* dynamicArrayV2Function()
{
	int* theArray = new int[10];

	for (int i = 0; i < 10; i++)
	{
		int* dynamicElement = new int;
		*dynamicElement = rand() % 101;
		theArray[i] = *dynamicElement;
	}

	for (int i = 0; i < 10; i++)
	{
		cout << theArray[i] << endl;
	}

	return theArray;
}

int main()

{
	srand(time(0));

	// Array as Pointer
	int randomArray[10];

	int* randomArrayPtr = randomArray;

	pointerArray(randomArrayPtr);

	for (int i = 0; i < 10; i++)
	{
		cout << randomArrayPtr[i] << endl;
	}

	// Dynamic Array
	int* dynamicArrayHolder;
	dynamicArrayHolder = dynamicArray();
	for (int i = 0; i < 10; i++)
	{
		cout << dynamicArrayHolder[i] << endl;
	}

	// Deleting Dynamic Elements
	int* dynamicArrayV2Holder;
	dynamicArrayV2Holder = dynamicArrayV2Function();
	for (int i = 0; i < 10; i++)
	{
		cout << dynamicArrayV2Holder[i] << endl;
	}

	delete[] dynamicArrayV2Holder;
	

	system("pause");
	return 0;
}